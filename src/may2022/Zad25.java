package may2022;

import java.util.Scanner;

class Zad25 {
    public static boolean isDividable(int num) {
        int n = num;

        while (n > 0) {
            int digit = n % 10;
            if (num % digit != 0) return false;
            n /= 10;
        }

        return true;
    }

    public static void main(String[] args) {
        int a;
        Scanner input = new Scanner(System.in);
        a = input.nextInt();
        if (isDividable(a)) System.out.println("Yes");
        else System.out.println("No");
    }
}
