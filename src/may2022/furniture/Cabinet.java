package may2022.furniture;

public class Cabinet extends Furniture {
    private int numberOfHinges;

    Cabinet(String typeProduct, Double productionPrice, int numberOfHinges) {
        super(typeProduct, productionPrice);
        this.numberOfHinges = numberOfHinges;
    }

    @Override
    public double priceClient() {
        return getProductionPrice() + getProductionPrice() * 0.15 + numberOfHinges * 4.50;
    }

    @Override
    public String toString() {
        return "The cabinet costs " + priceClient();
    }
}
