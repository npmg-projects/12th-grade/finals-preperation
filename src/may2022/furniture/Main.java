package may2022.furniture;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void print(List<Furniture> furnitures) {
        for (Furniture furniture : furnitures) {
            if (furniture.getTypeProduct().equals("table")) {
                System.out.println(furniture);
            }
        }
        for (Furniture furniture : furnitures) {
            if (furniture.getTypeProduct().equals("cabinet")) {
                System.out.println(furniture);
            }
        }
    }

    public static void main(String[] args) {
        List<Furniture> furnitureList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(" ");
        while (!input[0].equalsIgnoreCase("end")) {
            if (input.length == 2) {
                furnitureList.add(new Table(input[0], Double.valueOf(input[1])));
            } else if (input.length == 3) {
                furnitureList.add(new Cabinet(input[0], Double.valueOf(input[1]), Integer.parseInt(input[2])));
            }
            input = scanner.nextLine().split(" ");
        }
        print(furnitureList);
    }
}
