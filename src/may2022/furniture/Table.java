package may2022.furniture;

public class Table extends Furniture{
    Table(String typeProduct, Double productionPrice) {
        super(typeProduct, productionPrice);
    }

    @Override
    public double priceClient() {
        return getProductionPrice() + getProductionPrice() * 0.2;
    }

    @Override
    public String toString() {
        return "The table costs " + priceClient();
    }
}
