package may2022.furniture;

public abstract class Furniture {
    private String typeProduct;

    private Double productionPrice;

    Furniture(String typeProduct, Double productionPrice) {
        this.typeProduct = typeProduct;
        this.productionPrice = productionPrice;
    }

    public Double getProductionPrice() {
        return productionPrice;
    }

    public String getTypeProduct() {
        return typeProduct;
    }

    public void setProductionPrice(Double productionPrice) {
        this.productionPrice = productionPrice;
    }

    public void setTypeProduct(String typeProduct) {
        this.typeProduct = typeProduct;
    }

    public abstract double priceClient();
}
