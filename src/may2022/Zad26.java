package may2022;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zad26 {
    private static List<String> elements;


    public static void add(String[] input) {
        for (int i = 1; i < input.length; i++) {
            elements.add(input[i]);
        }
    }

    public static void update() {
        int index = 0;
        for (String element : elements) {
            element = element.substring(0, 1).toUpperCase() + element.substring(1);
            elements.set(index, element);
            index++;
        }
    }

    public static void remove(String[] command) {
        int n = Integer.parseInt(command[1]);
        if (n <= elements.size()) {
            elements.remove(n);
        }
    }

    public static void search(String[] command) {
        boolean hasMatch = false;
        String search = command[1];
        for (String element : elements) {
            if (element.equals(search)) {
                hasMatch = true;
                System.out.println(element);
            }
        }
        if (!hasMatch) System.out.println("Not contained.");
    }

    public static void length(String[] command) {
        boolean hasMatch = false;
        int n = Integer.parseInt(command[1]);
        for (String element : elements) {
            if (element.length() == n) {
                hasMatch = true;
                System.out.print(element + "-");
            }
        }
        System.out.println();
        if (!hasMatch) System.out.println("Not contained.");
    }

    public static void insert(String[] command) {
        int n = Integer.parseInt(command[1]);
        String word = command[2];
        try {
            elements.add(n, word);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Index out of bounds.");
        }
    }

    public static void print() {
        elements.forEach(e -> System.out.print(e + "; "));
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String[] command = input.nextLine().split(" ");
        elements = new ArrayList<>();
        while (!command[0].equals("END")) {
            switch (command[0]) {
                case "Add":
                    add(command);
                    break;
                case "Update":
                    update();
                    break;
                case "Remove":
                    remove(command);
                    break;
                case "Search":
                    search(command);
                    break;
                case "Length":
                    length(command);
                    break;
                case "Insert":
                    insert(command);
                    break;
                case "Print":
                    print();
                    break;
            }
            command = input.nextLine().split(" ");
        }
    }
}
