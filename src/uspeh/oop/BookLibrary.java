package uspeh.oop;

import uspeh.FileOperations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BookLibrary {
    private List<Book> books;
    private final FileOperations fileOperations;

    public BookLibrary() {
        books = new ArrayList<>();
        fileOperations = new FileOperations();
    }

    public void addBook(Book book) {
        if (!books.contains(book)) {
            books.add(book);

            try {
                fileOperations.writeFile("out.txt", book.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void getBook(String name) {
        for (Book book : books) {
            if (book.getTitle().equals(name)) {
                System.out.println(book);
            }
        }
    }

    public void getBook(String name, String author) {
        for (Book book : books) {
            if (book.getTitle().equals(name) && book.getAuthor().equals(author)) {
                System.out.println(book);
            }
        }
    }

    public void deleteBook(String name, String author) {
        for (Book book : books) {
            if (book.getTitle().equals(name) && book.getAuthor().equals(author)) {
                books.remove(book);
            }
        }
    }

    public int size() {
        return books.size();
    }
}
