package uspeh.oop;


import java.util.Scanner;

public class Zad2710 {
    public static void main(String[] args) {
        BookLibrary bookLibrary = new BookLibrary();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            String title = sc.next();
            String author = sc.next();
            bookLibrary.addBook(new Book(title, author));
        }
        bookLibrary.size();
    }
}
