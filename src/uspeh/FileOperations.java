package uspeh;

import java.io.*;
import java.util.List;

public class FileOperations {
    public List<String> readFile(String fileName) throws IOException {
        File file = new File(fileName);
        FileReader fileReader = new FileReader(file);
        try(BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader.lines().toList();
        }
    }

    public void writeFile(String fileName, String content) throws IOException {
        File file = new File(fileName);
        FileWriter fileReader = new FileWriter(file, true);
        try(BufferedWriter bufferedReader = new BufferedWriter(fileReader)) {
            bufferedReader.append(content).append("\n");
        }
    }

    public void deleteFromFile(String fileName, String content) throws IOException {
        File originalFile = new File(fileName);
        File tempFile = new File(fileName + ".tmp");

        BufferedReader reader = new BufferedReader(new FileReader(originalFile));
        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
        String line;

        while ((line = reader.readLine()) != null) {
            if (!line.contains(content)) {
                writer.write(line + System.lineSeparator());
            }
        }

        if (originalFile.delete()) {
            tempFile.renameTo(originalFile);
        } else {
            System.out.println("Could not delete original file");
        }
    }
}
