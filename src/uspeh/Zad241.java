package uspeh;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Zad241 {

    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        List<Integer> el = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            el.add(sc.nextInt());
        }
        el.sort(null);
        int diff = Math.abs(el.get(0) - el.get(1));
        for (int i = 0; i < el.size() - 1; i++) {
            int tempDiff = Math.abs(el.get(i) - el.get(i + 1));
            if (tempDiff < diff) {
                diff = tempDiff;
            }
        }
        System.out.println(diff);
    }
}
